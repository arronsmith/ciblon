#include <Wire.h>
#include <Ethernet.h>
#include <EEPROM.h>
#include <OSCmessage.h>
#include <OSCData.h>
#include <OSCMatch.h>
#include <HX711.h>

/*Ethernet/OSC things*/
int deviceID = EEPROM.read(0x00);
byte mac[] = {0x90, 0xA2, 0xDA, 0x0D, 0xCB, deviceID}; //MAC address.
IPAddress ip(10, 101, 101, deviceID); //IP address.
EthernetUDP udpClient; //client for tx/rx of OSC messages
IPAddress oscDestinationIP(10, 101, 101, 254);
char outgoingAddress[128]; //Buffer for assembling our outgoing OSC addresses..
#define OSC_LISTEN_PORT 12345
#define OSC_SEND_PORT 12345

/*Wind Sensor things*/
//XLR Pinout:
//1: GND (Black)
//2: +V (Red)
//3: Signal (Yellow)
#define WIND_SENSOR_PIN A0

/*Load Cell Things*/
//Load Cell Pinout:
// |Cell  |Cable  |HX711
// |Red   |Red    |E+
// |Black |Black  |E-
// |Green |Blue   |A+
// |White |Yellow |A-
 
HX711 loadCell(8, 9);

//sends OSC message with integer payload
void oscSendWithInt(char *address, int payload, int port = OSC_SEND_PORT) {
    OSCMessage msg(address);
    udpClient.beginPacket(oscDestinationIP, port);
    msg.add(payload);
    msg.send(udpClient);
    udpClient.endPacket();
    msg.empty();
}

void oscSendWithFloat(char *address, float payload, int port = OSC_SEND_PORT) {
    OSCMessage msg(address);
    udpClient.beginPacket(oscDestinationIP, port);
    msg.add(payload);
    msg.send(udpClient);
    udpClient.endPacket();
    msg.empty();
}

void tare(OSCMessage &msg) {
    loadCell.tare();
}

void setup()
{
    EEPROM.write(0x00, 10);
    
    Serial.begin(9600);
    Serial.println("Starting.");
    
    Serial.println("Initialising ethernet.");
    //Initialise our Ethernet if
    Ethernet.begin(mac, ip);
    udpClient.begin(OSC_LISTEN_PORT);
    
    Serial.println("Taring load cell.");
    loadCell.tare();
    
    Serial.println("Done");
}

void loop()
{
    OSCMessage m;
    int size;
    if ((size = udpClient.parsePacket()) > 0) {
        while (size--) {
            m.fill(udpClient.read());
        }
        //incoming OSC message handling:
        if (! m.hasError()) {
            m.dispatch("/ciblon/stretch/tare", tare);
        }
    }
    
    sprintf(outgoingAddress, "/ciblon/wind/raw/");
    oscSendWithInt(outgoingAddress, analogRead(A0));
    
    sprintf(outgoingAddress, "/ciblon/stretch/raw/");
    oscSendWithFloat(outgoingAddress, -loadCell.get_value());

}
